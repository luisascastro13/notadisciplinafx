package notadisciplina;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class TelaCadastroController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField nomeAluno, nota1, nota2, nota3, historico1, historico2, historico3;
    
    @FXML
    private Button cadastrar;
    
    @FXML
    private Label mensagem;
    
    
    @FXML
    public void cadastrar(ActionEvent event)
    {   
        Aluno matriz[] = NotaDisciplina.matriz;
        int i = NotaDisciplina.quantAluno; 
        matriz[i] = new Aluno();
        matriz[i].nome = nomeAluno.getText();
       // System.out.println(matriz[quantAluno].nome);
        
        matriz[i].nota1 = Double.parseDouble(nota1.getText());
        matriz[i].nota2 = Double.parseDouble(nota2.getText());
        matriz[i].nota3 = Double.parseDouble(nota3.getText());
        
//        System.out.println(matriz[quantAluno].nota1 + " " + matriz[quantAluno].nota2 + " " +matriz[quantAluno].nota3);
        
        matriz[i].hist1 = Double.parseDouble(historico1.getText());
        matriz[i].hist2 = Double.parseDouble(historico2.getText());
        matriz[i].hist3 = Double.parseDouble(historico3.getText());
        
        
        //System.out.println(matriz[quantAluno].hist1 + " " + matriz[quantAluno].hist2 + " " + matriz[quantAluno].hist3);
        NotaDisciplina.quantAluno++;
        NotaDisciplina.trocaTela("TelaInicial.fxml");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
}
