package notadisciplina;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class TelaInicialController implements Initializable {
    
    @FXML
    private Label historicoTurma, historicoAluno, trimestreAluno;
    @FXML
    private TextField nomeAluno;
    
    @FXML
    private Button mostrarHistoricoAluno, cadastrarAluno;
    
    @FXML
    private void cadastrarAluno(ActionEvent event) {
        
        NotaDisciplina.trocaTela("TelaCadastro.fxml");
    }
    
    @FXML
    public void mostrarMatriz(ActionEvent event)
    {
        String estringue = "";
        Aluno matriz[] = NotaDisciplina.matriz;
        for(int i=0; i<NotaDisciplina.quantAluno; i++)
        {
            estringue += matriz[i];
        }
        historicoTurma.setText(estringue);
    }
    
    @FXML
    public void calcularTrimestre(ActionEvent event)
    {
        Aluno aluninho = null;
        double valorTrimestre;
        for(int i=0; i<NotaDisciplina.quantAluno; i++)
        {
            aluninho = NotaDisciplina.matriz[i].acharAluno(nomeAluno.getText());
        }
        valorTrimestre = aluninho.getMedia();
        trimestreAluno.setText("" + valorTrimestre);
            
    }
    
    @FXML
    public void mostrarHistoricoAluno(ActionEvent event)
    {
        Aluno aluninho = null;
        int i=0;
        for(i=0; i<NotaDisciplina.quantAluno; i++)
        {
            aluninho = NotaDisciplina.matriz[i].acharAluno(nomeAluno.getText());
        }
        
        if(aluninho != null)
        {
            historicoAluno.setText("" + aluninho);
        }       
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        //cadastrar aluno, mostrar hist ind, mostrar hist turma e calcular trimestre da turma
    }
}