package notadisciplina;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class NotaDisciplina extends Application {
    private static Stage stage;
    public static int quantAluno=0;
    public static Aluno matriz[];
    
    public static Stage getStage() {
        return stage;
    }    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(NotaDisciplina.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        NotaDisciplina.stage = stage;
        stage.setScene(scene);
        stage.show();
    }
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        matriz = new Aluno[30];
        quantAluno = 0;
        Parent root = FXMLLoader.load(getClass().getResource("TelaInicial.fxml"));
        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }    
}
