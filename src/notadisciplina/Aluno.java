package notadisciplina;

public class Aluno {
    String nome;
    double hist1, hist2, hist3, nota1, nota2, nota3;
    
    Aluno(){
        
    }           

    void setNome(String texto) {
        nome = texto;
    }
    
    public double getMedia(){
        double media=0;
        
        media = 2/nota1 + 4/nota2 + 4/nota3;
        media = 10/media;        
        return media;
    }
    
    @Override
    public String toString()
    {
        return "\nNome" + nome + "\nHistórico" + hist1 + " "+ hist2 + " "+ hist3 + "notas" + nota1 + " " + nota2 + " "+nota3;
    }
    
    public Aluno acharAluno(String nome)
    {
        Aluno aluno = null;
        boolean nomeEstaNaMatriz = false;
        for(int i=0; i<NotaDisciplina.quantAluno; i++)
        {
            if(nome.equals(NotaDisciplina.matriz[i].nome))
            {
               aluno = NotaDisciplina.matriz[i];
               nomeEstaNaMatriz = true;
               break;
            }
        }
        if(!nomeEstaNaMatriz)
        {
            //System.out.println("Erro. Este nome não está cadastrado");
        }   
        return aluno;
    }
}
